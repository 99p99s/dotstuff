#!/usr/bin/env bash


# Shorter version of a common command that it used herein.
_checkexec() {
	command -v "$1" > /dev/null
}

# Handle proxy settings
export default_proxy="proxy.cab.cnea.gov.ar:3128"
proxy_on() {
	if (( $# > 0 )); then
		valid=$(echo $@ | sed -n 's/\([0-9]\{1,3\}.\?\)\{4\}:\([0-9]\+\)/&/p')
		if [[ $valid != $@ ]]; then
			>&2 echo "Invalid address."
			return 1
		fi
		local proxy=$1
		export http_proxy=$proxy \
			https_proxy=$proxy \
			ftp_proxy=$proxy \
			no_proxy="localhost,127.0.0.0/16,::1,10.0.0.0/16,0.0.0.0/16"
		echo "Proxy environment variable set."
		return 0
	else
		echo -n "username -> "; read username
		local pre=""
		if [[ $username != "" ]]; then
			echo -n "password -> "
			read -es password
			pre="$username:$password@"
		fi
		echo -n "server -> "; read server
		if [[ $server != "" ]]; then
			echo -n "port -> "
			read port
			local proxy=$pre$server:$port
			export http_proxy=$proxy \
				https_proxy=$proxy \
				ftp_proxy=$proxy \
				no_proxy="localhost,127.0.0.0/8,::1,10.0.0.0/16,0.0.0.0/16"
			echo "Proxy environment variables set."
			return 0
		fi
		echo "Using default proxy settings."
		export http_proxy=http://$default_proxy/
		export https_proxy=http://$default_proxy/
		export ftp_proxy=http://$default_proxy/
		export no_proxy="localhost,127.0.0.0/8,::1,10.0.0.0/16,0.0.0.0/16,.cab.cnea.gov.ar,.cnea.gov.ar"
		echo "Proxy environment variable set."
		return 0
	fi
}
proxy_off() {
	unset http_proxy https_proxy ftp_proxy no_proxy
	echo "Proxy environment variables removed."
}

# Colourise man pages
man() {
	env \
	LESS_TERMCAP_mb=$(tput bold; tput setaf 6) \
	LESS_TERMCAP_md=$(tput bold; tput setaf 6) \
	LESS_TERMCAP_me=$(tput sgr0) \
	LESS_TERMCAP_se=$(tput rmso; tput sgr0) \
	LESS_TERMCAP_ue=$(tput rmul; tput sgr0) \
	LESS_TERMCAP_us=$(tput smul; tput bold; tput setaf 4) \
	LESS_TERMCAP_mr=$(tput rev) \
	LESS_TERMCAP_mh=$(tput dim) \
	LESS_TERMCAP_ZN=$(tput ssubm) \
	LESS_TERMCAP_ZV=$(tput rsubm) \
	LESS_TERMCAP_ZO=$(tput ssupm) \
	LESS_TERMCAP_ZW=$(tput rsupm) \
		man "$@"
}

# Enter directory and list contents
cdc() {
	if [ -n "$1" ]; then
		builtin cd "$@" && ls -sAil --color=auto --group-directories-first
	else
		builtin cd ~ && ls -sAil --color=auto --group-directories-first
	fi
}

# Back up a file. Usage "backupthis <filename>"
backupthis() {
	cp -riv $1 ${1}-$(date +%Y%m%d%H%M).backup;
}

### ARCHIVE EXTRACTION ###
# usage: ex <file>
extract () {
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2|*.tbz2) tar xjfv $1   ;;
      *.tar.gz|*.tgz)   tar xzfv $1   ;;
      *.bz2)            bunzip2 $1   ;;
      *.rar)            unrar x $1     ;;
      *.gz)             gunzip $1    ;;
      *.tar)            tar xf $1    ;;
      *.zip)            unzip $1     ;;
      *.Z)              uncompress $1;;
      *.7z)             7z x $1      ;;
      *)                echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# Disable ctrl-s and ctrl-q.
stty -ixon

# ´cd´ into directory merely by typing the directory name.
shopt -s autocd

shopt -s expand_aliases

# Infinite history.
HISTSIZE=-1
HISTFILESIZE=-1

# Don't put duplicate lines or lines starting with space in the history.
# See `man bash` for more options.
HISTCONTROL=ignoreboth

# Append to the history file, don't overwrite it.
shopt -s histappend

# Check the window size after each command and, if necessary, update the
# values of LINES and COLUMNS.
shopt -s checkwinsize

# Needed for git status in the prompt
source /usr/share/git/git-prompt.sh
export GIT_PS1_SHOWDIRTYSTATE=1
export GIT_PS1_SHOWSTASHSTATE=1
export GIT_PS1_SHOWUNTRACKEDFILES=1

# Default pager.  Note that the option I pass to it will quit once you
# try to scroll past the end of the file.
export PAGER="less"
export MANPAGER="$PAGER"

#export PS1='\n\[\e[31;1m\](\[\e[36;1m\]\u\[\e[31;1m\])-(\[\e[34;1m\]\h\[\e[31;1m\])-(\[\e[37;1m\]\w\[\e[31;1m\])-(\[\e[37;1m\]\[\033[0;35m\]$(__git_ps1 "%s")\[\033[2;33m\]\[\e[31;1m\])\n\[\e[31;1m\](\[\e[33;1m\]\!\[\e[31;1m\]) \[\e[0;34;1m\](~˘▾˘)~ \[\e[0;31;1m\]=\[\e[0;32;1m\]=\[\e[0;33;1m\]=\[\e[0;34;1m\]=\[\e[35;1m\]> \[\e[36;1m\]'
#export PS2="\[\e[0;31;1m\]=\[\e[0;32;1m\]=\[\e[0;33;1m\]=\[\e[0;34;1m\]=\[\e[35;1m\]> \[\e[0m\]"

#export PATH=$HOME/cmus/bin:$PATH
#export PATH=$HOME/.config/bspwm/panel:$PATH
export _JAVA_AWT_WM_NONREPARENTING=1

#PS1="\n\[\e[31;1m\]┌─╼[\[\e[36;1m\]\u\[\e[31;1m\]]──[\[\e[34;1m\]\h\[\e[31;1m\]]──[\[\e[37;1m\]\w\[\e[31;1m\]]──[\[\e[37;1m\]\[\033[0;35m\]$(__git_ps1 "%s")\[\033[2;33m\]\[\e[31;1m\]]\n\[\e[31;1m\]└────╼[\[\e[33;1m\]\!\[\e[31;1m\]]\[\e[0;31;1m\]─\[\e[0;32;1m\]─\[\e[0;33;1m\]─\[\e[0;34;1m\]─\[\e[35;1m\]> \[\e[0;34;1m\](~˘▾˘)~ \[\e[36;1m\]"
export PS1="\n\[\e[31;1m\]┌─╼[\[\e[36;1m\]\u\[\e[31;1m\]]──[\[\e[34;1m\]\h\[\e[31;1m\]]──[\[\e[37;1m\]\w\[\e[31;1m\]]\n\[\e[31;1m\]└─╼[\[\e[33;1m\]\!\[\e[31;1m\]]\[\e[0;31;1m\]─\[\e[0;32;1m\]─\[\e[0;33;1m\]─\[\e[0;34;1m\]─\[\e[35;1m\]> \[\e[0;34;1m\](~˘▾˘)~ \[\e[37;1m\]"
export PS2="\[\e[0;31;1m\]=\[\e[0;32;1m\]=\[\e[0;33;1m\]=\[\e[0;34;1m\]=\[\e[35;1m\]> \[\e[36;1m\]"

# Enable tab completion when starting a command with 'sudo'
[ "$PS1" ] && complete -cf sudo

#eval "`dircolors`"
# Enable automatic color support for common commands that list output
# and also add handy aliases.  Note the link to the `dircolors`.  This
# is provided by my dotfiles.
if _checkexec dircolors; then
	dircolors_data="$HOME/.local/share/my_bash/dircolors"
	test -r $dircolors_data && eval "$(dircolors -b ${dircolors_data})" || eval "$(dircolors -b)"
fi

# Load shortcuts & aliases
[ -f "$HOME/.config/shortcutrc" ] && source "$HOME/.config/shortcutrc"
[ -f "$HOME/.config/aliasrc" ] && source "$HOME/.config/aliasrc"


#	Colors:

#  BLACK=	'\e[0;30m'
#  RED=		'\e[0;31m'
#  GREEN=	'\e[0;32m'
#  YELLOW=	'\e[0;33m'
#  BLUE=	'\e[0;34m'
#  MAGENT=	'\e[0;35m'
#  CYAN=	'\e[0;36m'
#  WHITE=	'\e[0;37m'

#  LIGHTBLACK=	'\e[1;30m'
#  LIGHTRED=	'\e[1;31m'
#  LIGHTGREEN=	'\e[1;32m'
#  LIGHTYELLOW=	'\e[1;33m'
#  LIGHTBLUE=	'\e[1;34m'
#  LIGHTMAGENT= '\e[1;35m'
#  LIGHTCYAN=	'\e[1;36m'
#  LIGHTWHITE=	'\e[1;37m'


# When I need to copy the contents of a file to the clipboard
if _checkexec xclip; then
	alias xclipc='xclip -selection clipboard' # followed by path to file
fi
